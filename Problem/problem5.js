// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

// Import modules here
const problem4 = require("../Problem/problem4");

function problem5(inventory) {
  let allCarYears = problem4(inventory);
  let allOldCars = allCarYears.filter((year) => {
    return year < 2000;
  });

  return allOldCars;
}

module.exports = problem5;
