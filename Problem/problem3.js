// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function problem3(inventory) {
  let allCarModels = inventory
    .map((obj) => {
      return obj.car_model;
    })
    .sort((a, b) => {
      return a.toLowerCase().localeCompare(b.toLowerCase());
    });

  return allCarModels;
}

module.exports = problem3;
